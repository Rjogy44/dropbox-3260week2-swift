//
//  ViewController.swift
//  ScorePongSwift
//
//  Created by Randy Jorgensen on 1/25/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var lbl1: UILabel!
    @IBAction func reset(sender: AnyObject) {
        
        self.lbl2.text = "0";
        self.lbl1.text = "0";
        
        
    }
    @IBOutlet var stepper2: UIStepper!
    
    @IBAction func stepper2(sender1: UIStepper) {
        
        self.lbl2.text = "\(Int(sender1.value))"
    }
    @IBOutlet var txt1: UIView!
    @IBOutlet weak var txt2: UITextField!
    @IBOutlet weak var lbl2: UILabel!

    @IBOutlet var stepper1: UIStepper!
    
    @IBAction func stepper1(sender: UIStepper){
        
        self.lbl1.text = "\(Int(sender.value))"
    }
    


    override func viewDidLoad() {
        super.viewDidLoad()
    

        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

